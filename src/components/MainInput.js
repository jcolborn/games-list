import React from "react";

class MainInput extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="input-block">
        <input
          class="rounded"
          id="gametitle"
          type="text"
          placeholder="Game Title"
          onChange={this.props.onChange}
          value={this.props.gameTitle}
        />
        <input
          class="rounded"
          id="date"
          type="date"
          onChange={this.props.onChange}
          value={this.props.date}
        />
        <label for="digital">
          <input
            class="checkbox"
            id="digital"
            type="checkbox"
            onChange={this.props.onChange}
            checked={this.props.digital}
          />
          Digital?
        </label>
        <input
          class="rounded"
          id="platform"
          type="text"
          placeholder="Platform"
          onChange={this.props.onChange}
          value={this.props.platform}
        />
        <input
          class="rounded"
          id="enter"
          type="submit"
          text="add"
          onClick={this.props.onSubmit}
        />
      </div>
    );
  }
}

export default MainInput;
