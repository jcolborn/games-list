import React from "react";

class GamesList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.games == null) {
      return null;
    } else {
      return (
        <div>
          <table className="parenttable">
            <th>Game</th>
            <th>Digital</th>
            <th>Date of Purchase</th>
            <th>Platform</th>
            {this.props.games.map(element => (
              <tr>
                <td>{element.gameTitle}</td>
                <td>{element.digital ? "yes" : "no"}</td>
                <td>{element.date}</td>
                <td>{element.platform}</td>
              </tr>
            ))}
          </table>
        </div>
      );
    }
  }
}

export default GamesList;
