import React, { Component } from "react";
import logo from "./logo.svg";
import MainInput from "./components/MainInput";
import GamesList from "./components/GamesList";
import "./App.css";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gameTitle: this.gameTitle,
      date: this.date,
      digital: this.digital,
      platform: this.platform,
      games: this.games
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange = event => {
    switch (event.target.id) {
      case "gametitle":
        this.setState({ gameTitle: event.target.value });
        break;
      case "date":
        this.setState({ date: event.target.value });
        break;
      case "digital":
        this.setState({ digital: event.target.checked });
        break;
      case "platform":
        this.setState({ platform: event.target.value });
        break;
    }
  };

  onSubmit = event => {
    event.preventDefault();

    var today = new Date();
    var year = today.getFullYear();
    var month = today.getMonth() + 1;
    var date = today.getDate();

    var todayDate = year + "-" + month + "-" + date;

    if (
      this.state.gameTitle === undefined ||
      this.state.date === undefined ||
      this.state.platform === undefined
    ) {
      alert("Error: undefined element");
    } else {
      if (this.state.games == null) {
        this.setState({
          games: [
            {
              gameTitle: this.state.gameTitle,
              date: this.state.date,
              digital: this.state.digital,
              platform: this.state.platform
            }
          ]
        });
      } else {
        this.setState({
          games: [
            ...this.state.games,
            {
              gameTitle: this.state.gameTitle,
              date: this.state.date,
              digital: this.state.digital,
              platform: this.state.platform
            }
          ]
        });
      }
    }

    this.setState({
      gameTitle: "",
      date: todayDate,
      digital: false,
      platform: ""
    });
  };

  render() {
    return (
      <div>
        <h1 class="title">Games List</h1>
        <p class="filler">Please enter your games below</p>
        <MainInput
          gameTitle={this.state.gameTitle}
          date={this.state.date}
          digital={this.state.digital}
          platform={this.state.platform}
          onChange={this.onChange}
          onSubmit={this.onSubmit}
        />
        <GamesList games={this.state.games} />
      </div>
    );
  }
}

export default App;
