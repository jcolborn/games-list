This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## The Application

This application takes in user data in the form of a game list. It then stores the data as an array of Objects in React state. Once data is stored then the state is then written out as a table. Currently the data only lives in state so there is no permanance.

## React

React is used to build state, a dynamic list in the form of a table, and a dynamic input that clears itself on entry. The form created is a list of inputs that collect the data and pass it to the application through props. This happens from `MainInput.js` where the inputs are stored. The data is collected in state within `App.js`. The data is passed into `GamesList.js` as props to create the list.

## The List

The list is formed in `GamesList.js`. The list will not create unless data is avaialbe in the list. This is done by checking the prop and see if it is null. It will be null until data is entered into the list. The list then loops through the array of objects and outputs the array into a table.
